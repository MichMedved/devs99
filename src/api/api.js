import axios from 'axios'

const url = 'http://devs99.org/wp-json/wp/v2/frontend'

const handleError = (response) => {
  return response.status === 200 ? response : Promise.reject(response.statusText)
}

const unwrapData = (response) => response.json()

export default {
  get (fullUrl) {
    return axios.get(url + fullUrl)
      .then(handleError)
      .then(unwrapData)
      .catch(error => { throw new Error(error) })
  }
}
