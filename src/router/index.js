import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/routes/Home'
import vacancy from '@/components/routes/Vacancy'
import aboutus from '@/components/routes/aboutUs'
import brief from '@/components/routes/brief'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/aboutUs', component: aboutus },
    { path: '/brief', component: brief },
    { path: '/vacancy', component: vacancy }
  ]
})
