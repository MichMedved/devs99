import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as types from './mutations'

Vue.use(Vuex, axios)

const store = new Vuex.Store({
  state: {
    galleryJsonPhoto: {},
    headerObject: {},
    callUi_design: {},
    url: ''
  },
  getters: {
    getObjectGalleryPhoto (state) {
      return state.galleryJsonPhoto
    },
    getHeaderObject (state) {
      return state.headerObject
    },
    callUi_design (state) {
      return state.callUi_design
    }
  },

  // mutations
  mutations: {
    [types.setCallFirstPhoto] (state, massPhotoJson) {
      for (let key in massPhotoJson.acf) {
        state.callUi_design = massPhotoJson.acf[key]
        break
      }
    },
    [types.setGalleryJsonPhoto] (state, resJson) {
      let reg = /_development\b|_design\b/
      for (let key in resJson.acf) {
        if (key.match(reg)) {
          Vue.set(state.galleryJsonPhoto, key, resJson.acf[key])
        }
      }
      // console.log(state.galleryJsonPhoto)
    },
    [types.setHeaderObject] (state, header) {
      state.headerObject = header.acf
      console.log(state.headerObject)
    }
  },
  actions: {
    getDataJson ({commit, state}, link = '') {
      state.url = link
      const fullUrl = `http://wp-test.99devsworkspace.optare.de/wp-json/wp/v2/frontend/${state.url}`
      axios.get(fullUrl)
        .then(response => {
          commit('setGalleryJsonPhoto', response.data)
          commit('setHeaderObject', response.data)
          commit('setCallFirstPhoto', response.data)
        })
        .catch(error => {
          /* delete code down below for productions */
          axios.get(`http://devs99.org/wp-json/wp/v2/frontend/${state.url}`)
            .then(response => {
              commit('setGalleryJsonPhoto', response.data)
              commit('setHeaderObject', response.data)
              commit('setCallFirstPhoto', response.data)
            })
            .catch(error => {
              console.log(error)
            })
          /* for end */
          console.log(error.response.statusText)
        })
    }
  }
})

export default store
