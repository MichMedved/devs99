import axios from 'axios'
// import api from '../../utils/api'

export default {
  getDataJson ({ commit }, link = '') {
    const url = link
    const fullUrl = `${url}`
    axios.get(fullUrl)
      .then(response => {
        commit('setGalleryJsonPhoto', response.data)
        commit('setHeaderObject', response.data)
        commit('setCallFirstPhoto', response.data)
      })
      .catch(error => {
        console.log(error)
      })
  }
}
